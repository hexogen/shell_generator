<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class BashCreationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @test
     * @return void
     */
    public function itShouldCreateAScript()
    {
        $response = $this->postJson('/bash-script/create', [
            "filename" => "myscript.sh",
            "tasks" => [
                [
                    "name" => "rm",
                    "command" => "rm -f /tmp/test",
                    "dependencies" => [
                        "cat"
                    ]
                ],
                [
                    "name" => "cat",
                    "command" => "cat /tmp/test",
                    "dependencies" => [
                        "chown",
                        "chmod"
                    ]
                ],
                [
                    "name" => "touch",
                    "command" => "touch /tmp/test"
                ],
                [
                    "name" => "chown",
                    "command" => "chmod 600 /tmp/test"
                ],
                [
                    "name" => "chmod",
                    "command" => "chown root:root /tmp/test"
                ]
            ]
        ]);

        $response->assertStatus(200);

        $response->assertJson(['success' => true]);

        $this->assertFileExists(storage_path('app/bash/myscript.sh'));

        Storage::delete('bash/myscript.sh');
    }
}
