<?php

namespace Tests\Unit\Support;

use App\Exceptions\TasksIntegrityViolation;
use App\Support\TasksValidator;
use PHPUnit\Framework\TestCase;

class TasksValidatorTest extends TestCase
{
    /**
     * @var TasksValidator|mixed
     */
    private $tasksValidator;

    public function setUp(): void
    {
        parent::setUp();
        $this->tasksValidator = app()->make(TasksValidator::class);
    }

    /**
     * @test
     */
    public function itShouldCheckForTasksNamesDuplication()
    {
        $tasks = [
            [
                "name" => "cat",
                "command" => "cat /tmp/test",
                "dependencies" => [
                    "chown",
                    "chmod"
                ]
            ],
            [
                "name" => "chmod",
                "command" => "chown root:root /tmp/test/super"
            ],
            [
                "name" => "touch",
                "command" => "touch /tmp/test"
            ],
            [
                "name" => "chown",
                "command" => "chmod 600 /tmp/test"
            ],
            [
                "name" => "chmod",
                "command" => "chown root:root /tmp/test"
            ]
        ];

        $this->expectException(TasksIntegrityViolation::class);

        $this->tasksValidator->checkIntegrity($tasks);
    }

    /**
     * @test
     */
    public function itShouldCheckForUnknownDependencies()
    {
        $tasks = [
            [
                "name" => "cat",
                "command" => "cat /tmp/test",
                "dependencies" => [
                    "chown",
                    "chmod",
                    "init"
                ]
            ],
            [
                "name" => "touch",
                "command" => "touch /tmp/test"
            ],
            [
                "name" => "chown",
                "command" => "chmod 600 /tmp/test"
            ],
            [
                "name" => "chmod",
                "command" => "chown root:root /tmp/test"
            ]
        ];

        $this->expectException(TasksIntegrityViolation::class);

        $this->tasksValidator->checkIntegrity($tasks);
    }

    /**
     * @test
     */
    public function itShouldCheckForDependenciesLoops()
    {
        $tasks = [
            [
                "name" => "cat",
                "command" => "cat /tmp/test",
                "dependencies" => [
                    "chown",
                    "chmod",
                    "dog",
                ]
            ],
            [
                "name" => "dog",
                "command" => "dog /tmp/test",
                "dependencies" => [
                    "cat",
                ]
            ],
            [
                "name" => "touch",
                "command" => "touch /tmp/test"
            ],
            [
                "name" => "chown",
                "command" => "chmod 600 /tmp/test"
            ],
            [
                "name" => "chmod",
                "command" => "chown root:root /tmp/test"
            ]
        ];

        $this->expectException(TasksIntegrityViolation::class);

        $this->tasksValidator->checkIntegrity($tasks);
    }
}
