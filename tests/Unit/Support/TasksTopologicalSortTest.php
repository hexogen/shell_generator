<?php

namespace Tests\Unit\Support;

use App\Support\TasksTopologicalSort;
use Illuminate\Support\Arr;
use PHPUnit\Framework\TestCase;

class TasksTopologicalSortTest extends TestCase
{
    /**
     * @var TasksTopologicalSort|mixed
     */
    private $topologicalSort;

    protected function setUp(): void
    {
        parent::setUp();

        $this->topologicalSort = app()->make(TasksTopologicalSort::class);
    }

    /**
     * @test
     */
    public function itShouldReturnTasksInTopologicalOrder()
    {
        $tasks = [
            [
                "name" => "rm",
                "command" => "rm -f /tmp/test",
                "dependencies" => [
                    "cat"
                ]
            ],
            [
                "name" => "cat",
                "command" => "cat /tmp/test",
                "dependencies" => [
                    "chown",
                    "chmod"
                ]
            ],
            [
                "name" => "touch",
                "command" => "touch /tmp/test"
            ],
            [
                "name" => "chown",
                "command" => "chmod 600 /tmp/test"
            ],
            [
                "name" => "chmod",
                "command" => "chown root:root /tmp/test"
            ]
        ];

        $tasks = $this->topologicalSort->sort($tasks);

        $tasksPositions = array_flip(Arr::pluck($tasks, 'name'));

        $this->assertGreaterThan($tasksPositions['cat'], $tasksPositions['rm']);
        $this->assertGreaterThan($tasksPositions['chown'], $tasksPositions['cat']);
        $this->assertGreaterThan($tasksPositions['chmod'], $tasksPositions['cat']);
    }
}
