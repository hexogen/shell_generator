<?php

namespace Tests\Unit\Support;

use App\Contracts\BashGenerator;
use PHPUnit\Framework\TestCase;

class BashGeneratorTest extends TestCase
{
    /**
     * @var BashGenerator|mixed
     */
    private $bashGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->bashGenerator = app()->make(\App\Support\BashGenerator::class);
    }

    /**
     * @test
     */
    public function itShouldBeAnInstanceOfBashGeneratorContract()
    {
        $this->assertInstanceOf(\App\Contracts\BashGenerator::class, $this->bashGenerator);
    }

    /**
     * @test
     */
    public function itShouldGenerateBashScript()
    {
        $commands = [
            'touch /tmp/test',
            'chmod 600 /tmp/test',
            'chown root:root /tmp/test',
            'cat /tmp/test',
            'rm -f /tmp/test',
        ];

        $bashString = $this->bashGenerator->generate($commands);

        $this->assertEquals('#!/bin/bash'
            . "\n\n"
            . "touch /tmp/test\n"
            . "chmod 600 /tmp/test\n"
            . "chown root:root /tmp/test\n"
            . "cat /tmp/test\n"
            . "rm -f /tmp/test\n", $bashString);
    }
}
