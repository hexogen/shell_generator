# Shell generator

## Install

```
#install dependencies
composer install
```

## Run

```
#run docker
./vendor/bin/sail build
./vendor/bin/sail up
```

```
#go inside docker container
./vendor/bin/sail shell
#inside container run
php -r "file_exists('.env') || copy('.env.example', '.env');"
php artisan key:generate
```


## Test

```
#run tests
php artisan test
```

Please note, functionality for one test is not implemented
and shows an error

## Api

```
POST /bash-script/create HTTP/1.1
Host: 0.0.0.0
Content-Type: application/json

{
    "filename": "myscript.sh",
    "tasks": [
        {
            "name": "rm",
            "command": "rm -f /tmp/test",
            "dependencies": [
                "cat"
            ]
        },
        {
            "name": "cat",
            "command": "cat /tmp/test",
            "dependencies": [
                "chown",
                "chmod"
            ]
        },
        {
            "name": "touch",
            "command": "touch /tmp/test"
        },
        {
            "name": "chown",
            "command": "chmod 600 /tmp/test"
        },
        {
            "name": "chmod",
            "command": "chown root:root /tmp/test"
        }
    ]
}
```

should create bash script inside ./storage/app/bash folder
