<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use App\Contracts\BashGenerator;
use App\Contracts\TasksValidator;
use App\Contracts\TasksSort;
use App\Exceptions\TasksIntegrityViolation;
use App\Http\Requests\BashSchemaRequest;
use Illuminate\Contracts\Filesystem\Filesystem;


class BashScriptCreator extends Controller
{
    /**
     * @var TasksSort
     */
    private TasksSort $tasksSort;

    /**
     * @var TasksValidator
     */
    private TasksValidator $tasksValidator;

    /**
     * @var BashGenerator
     */
    private BashGenerator $bashGenerator;

    /**
     * @var Filesystem
     */
    private Filesystem $filesystem;

    /**
     * BashScriptCreator constructor.
     *
     * @param TasksValidator $tasksValidator
     * @param TasksSort $tasksSort
     * @param BashGenerator $bashGenerator
     * @param Filesystem $filesystem
     */
    public function __construct(
        TasksValidator $tasksValidator,
        TasksSort $tasksSort,
        BashGenerator $bashGenerator,
        Filesystem $filesystem
    ) {
        $this->tasksValidator = $tasksValidator;
        $this->tasksSort = $tasksSort;
        $this->bashGenerator = $bashGenerator;
        $this->filesystem = $filesystem;
    }

    /**
     * Make bash script according to schema
     *
     * @param BashSchemaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(BashSchemaRequest $request)
    {
        $fileName = $request->json()->get('filename');
        $tasks = $request->json()->get('tasks');

        try {
            $this->tasksValidator->checkIntegrity($tasks);
            $tasks = $this->tasksSort->sort($tasks);

            $bash = $this->bashGenerator->generate(Arr::pluck($tasks, 'command'));

            $this->filesystem->put('bash/' . $fileName, $bash);
        } catch (TasksIntegrityViolation $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        } catch (\Exception $e) {
            logger()->error($e->getMessage(), ['exception' => $e]);

            return response()->json([
                'success' => false,
                'message' => 'Error occurred'
            ])->setStatusCode(500);
        }

        return response()->json([
            'success' => true,
            'message' => 'bash script successfully created'
        ]);
    }
}
