<?php

namespace App\Providers;

use App\Contracts\BashGenerator;
use App\Contracts\TasksSort;
use App\Contracts\TasksValidator;
use App\Support\TasksTopologicalSort;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        BashGenerator::class => \App\Support\BashGenerator::class,
        TasksSort::class => TasksTopologicalSort::class,
        TasksValidator::class => \App\Support\TasksValidator::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
