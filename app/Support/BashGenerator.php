<?php

namespace App\Support;

use App\Contracts\BashGenerator as BashGeneratorContract;

class BashGenerator implements BashGeneratorContract
{
    /**
     * @param array $commands
     * @return string
     */
    public function generate(array $commands): string
    {
        return "#!/bin/bash\n\n" . implode("\n", $commands) . "\n";
    }
}
