<?php

namespace App\Support;

use App\Contracts\TasksValidator as  TasksValidatorContract;
use App\Exceptions\TasksIntegrityViolation;

class TasksValidator implements TasksValidatorContract
{
    /**
     * @param array $tasks
     * @throws TasksIntegrityViolation
     * @return void
     */
    public function checkIntegrity(array $tasks)
    {
        $tasksNames = $this->getTasksList($tasks);

        $this->checkUnknownDependencies($tasks, $tasksNames);

        $this->checkDependenciesLoop($tasks);
    }

    /**
     * @param array $tasks
     * @return array
     * @throws TasksIntegrityViolation
     */
    private function getTasksList(array $tasks): array
    {
        $tasksNames = [];

        foreach ($tasks as $task) {
            if (isset($tasksNames[$task['name']])) {
                throw new TasksIntegrityViolation('Task name duplication');
            }

            $tasksNames[$task['name']] = true;
        }

        return $tasksNames;
    }

    /**
     * @param array $tasks
     * @param array $tasksNames
     * @throws TasksIntegrityViolation
     */
    private function checkUnknownDependencies(array $tasks, array $tasksNames): void
    {
        foreach ($tasks as $task) {
            if (isset($task['dependencies'])) {
                foreach ($task['dependencies'] as $dependency) {
                    if (!isset($tasksNames[$dependency])) {
                        throw new TasksIntegrityViolation('Unknown task dependency');
                    }
                }
            }
        }
    }

    /**
     * @param array $tasks
     */
    private function checkDependenciesLoop(array $tasks)
    {
        //TODO implement is DAG algorithm to prevent loops in dependencies
    }
}
