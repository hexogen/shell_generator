<?php

namespace App\Support;

use App\Contracts\TasksSort as TasksSortContract;
use Illuminate\Support\Arr;

class TasksTopologicalSort implements TasksSortContract
{
    private array $sortedTusks;

    private array $tasksDictionary;

    public function sort(array $tasks): array
    {
        $this->sortedTusks = [];
        $this->tasksDictionary = [];

        $this->prepareTasksDictionary($tasks);

        $this->sortTusks(Arr::pluck($tasks, 'name'));

        $result = [];

        foreach ($this->sortedTusks as $taskName => $val) {
            $result[] = $this->tasksDictionary[$taskName];
        }

        return $result;
    }

    private function prepareTasksDictionary(array $tasks)
    {
        foreach ($tasks as $task) {
            $this->tasksDictionary[$task['name']] = $task;
        }
    }

    private function sortTusks(array $tasksNames)
    {
        foreach ($tasksNames as $taskName) {
            if (isset($this->sortedTusks[$taskName])) {
                continue;
            }

            $task = $this->tasksDictionary[$taskName];

            if (isset($task['dependencies'])) {
                $this->sortTusks($task['dependencies']);
            }

            $this->sortedTusks[$taskName] = true;
        }
    }
}
