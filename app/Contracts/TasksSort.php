<?php

namespace App\Contracts;

interface TasksSort {

    /**
     * Figure out if
     *
     * @param array $tasks
     * @return array
     */
    public function sort(array $tasks): array;
}
