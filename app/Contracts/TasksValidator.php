<?php

namespace App\Contracts;

use App\Exceptions\TasksIntegrityViolation;

interface TasksValidator {

    /**
     * Figure out if
     *
     * @param array $tasks
     * @throws TasksIntegrityViolation
     * @return void
     */
    public function checkIntegrity(array $tasks);
}
