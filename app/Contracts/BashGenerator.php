<?php


namespace App\Contracts;


interface BashGenerator
{
    /**
     * @param array $commands
     * @return string
     */
    public function generate(array $commands): string;
}
